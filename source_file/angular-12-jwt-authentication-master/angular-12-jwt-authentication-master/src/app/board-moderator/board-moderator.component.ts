import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../_services/Employee';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css'],
})
export class BoardModeratorComponent implements OnInit {
  employee: Employee[] = [];
  student: Student_Class[] = [];
  content?: string;

  constructor(private userService: UserService, private router: Router) {}

  updateStudentById(id: number) {
    this.router.navigate(['updateStudentByMod', id]);
  }

  ngOnInit(): void {
    this.userService.getStudentAllData().subscribe(
      (data) => {
        this.student = data;
      },
      (err) => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}

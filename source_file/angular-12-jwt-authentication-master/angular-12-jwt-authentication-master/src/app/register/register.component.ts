import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  pass = 'Enter 8-Characters ex:" #-1-B-b-9* "';
  form: any = {
    username: null,
    email: null,
    password: null,
  };
  isSuccessful = false;
  registrationFailed = false;
  errorMessage = 'sOMe IssuE OccuR';

  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const { username, email, password } = this.form;

    this.authService.register(username, email, password).subscribe(
      (data) => {
        console.log(data);
        this.isSuccessful = true;
        this.registrationFailed = false;
      },
      (err) => {
        this.errorMessage = err.error.message;
        this.registrationFailed = true;
      }
    );
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentalltaskComponent } from './studentalltask.component';

describe('StudentalltaskComponent', () => {
  let component: StudentalltaskComponent;
  let fixture: ComponentFixture<StudentalltaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentalltaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentalltaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
